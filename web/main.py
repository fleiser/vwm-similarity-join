import os
import sys
import cv2 as cv
import numpy as np
from functools import reduce
from flask import Flask,render_template, flash, request, redirect, url_for
from werkzeug.utils import secure_filename
from libraries.knearestneighbors import KNearestNeighbors
from libraries.rangequery import RangeQuery
from libraries.histogramcalculation import HistogramCalculation
from libraries.siftcalculation import SIFTCalculation
from libraries.imageloader import ImageLoader

app = Flask(__name__)
app.secret_key = os.urandom(12).hex()
ALLOWED_EXTENSIONS = ('jpg', 'png')
image_histograms = []
sift_word_histograms = []
code_book = []
descriptors = []
hc = HistogramCalculation()
siftc = SIFTCalculation(1000)
histogram_knn = KNearestNeighbors(kneighbors=4)
sift_knn = KNearestNeighbors(kneighbors=4)
histogram_range_query = RangeQuery(range = 2.0)
sift_range_query = RangeQuery(range = 100.0)
path_prefix = 'static/dataset/precalculated/100_words_1000_features_original'

def load_images():
    loader = ImageLoader()
    global image_histograms
    global code_book
    global descriptors
    image_count, images = loader.get_images_root('./static/dataset/images/vac', limit=-1)
    images = sorted(images)
    print(image_count)
    print("Loading image histograms")
    if os.path.exists(os.path.join(path_prefix,'image_histograms.npy')):
        print("Histogram data file found")
        image_histograms = list(np.load(os.path.join(path_prefix,'image_histograms.npy')))
    else:
        for index, image in enumerate(images):
            print(index, image)
            image_histograms.append(hc.calculate_for_image(cv.imread(image), [0, 1, 2], (8,12,12), [0,180,0,256,0,256], hsv=True, resize=False))
        np.save(os.path.join(path_prefix,'image_histograms.npy'),image_histograms)
    print("Image histograms Loaded", len(image_histograms))
    histogram_knn.load(image_histograms, images)
    histogram_range_query.load(image_histograms, images)
    print("Loading SIFT kmeans codebook")
    
    print("loading vocabulary")
    if os.path.exists(os.path.join(path_prefix,'vocabulary.npy')) and os.path.exists(os.path.join(path_prefix,'code_book.npy')):
        print("vocabulary and code book data files found")
        vocabulary = np.load(os.path.join(path_prefix,'vocabulary.npy'))
        code_book = np.load(os.path.join(path_prefix,'code_book.npy'))
        siftc.add_code_book(code_book)
        print("code book shape:", siftc.code_book.shape)
    else:
        print("Loading descriptors")
        if os.path.exists(os.path.join(path_prefix,'descriptors.npy')):
            print("Descriptors found")
            descriptors = np.load(os.path.join(path_prefix,'descriptors.npy'))
        else:
            print("Calculating descriptors")
            for index, image in enumerate(images):
                print(index, image)
                descriptors.append(siftc.calculate_sift_descriptors(image, resize=False))
            np.save(os.path.join(path_prefix,'descriptors.npy'),descriptors)
        print("Calculating vocabulary")
        vocabulary = siftc.calculate_vocabulary(descriptors, k=100)
        np.save(os.path.join(path_prefix,'vocabulary.npy'),vocabulary)
        np.save(os.path.join(path_prefix,'code_book.npy'),siftc.code_book)
    sift_knn.load(vocabulary, images)
    sift_range_query.load(vocabulary, images)
    print("All loaded")


def allowed_filename(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        filenames = []
        input_histograms = []
        input_sift_histograms = []
        closest_images = []
        feature_extraction = request.form['extraction']
        distance_method = request.form['distance']
        n_neighbors = int(request.form['neighbors'])
        upper_range = float(request.form['range'])
        algorithm = request.form['algorithm']
        join = request.form['join']
        distances = dict()
        #save the uploaded images
        for file in request.files.getlist("file[]"):
                filename = secure_filename(file.filename)
                file.save(os.path.join("static/uploaded",filename))
                filenames.append(os.path.join("static/uploaded",filename))
        
        print(feature_extraction)
        print(distance_method)
        print(n_neighbors)
        print(upper_range)
        print(algorithm)
        print(join)

        if feature_extraction == 'SIFT':
            for image in filenames:
                print(image)
                input_sift_histograms.append(siftc.calculate_word_histogram_for_image(image, resize=False))
            
            if join == 'Union':
                for image in input_sift_histograms:
                    if algorithm == 'KNN':
                        neighbors = sift_knn.k_nearest(image, neighbors=n_neighbors, distance=distance_method)
                    elif algorithm == 'Range Query':
                        neighbors = sift_range_query.output(image, max_range=upper_range, distance=distance_method)

                    for neighbor in neighbors:
                        closest_images.append(neighbor.label)
                        if neighbor.label in distances:
                            distances[neighbor.label].append(neighbor.vector)
                        else:
                            distances[neighbor.label] = [neighbor.vector]
                return render_template('result.html', extraction=feature_extraction, algorithm=algorithm ,uploaded_images=filenames,nearest_images=closest_images, distances=distances)

            if join == 'Intersect':
                all_neighbors = []  
                for image in input_sift_histograms:
                    if algorithm == 'KNN':
                        neighbors = sift_knn.k_nearest(image, neighbors=n_neighbors, distance=distance_method)
                    elif algorithm == 'Range Query':
                        neighbors = sift_range_query.output(image, max_range=upper_range, distance=distance_method)
                    image_neighbors = set()
                    for neighbor in neighbors:
                        image_neighbors.add(neighbor.label)
                        if neighbor.label in distances:
                            distances[neighbor.label].append(neighbor.vector)
                        else:
                            distances[neighbor.label] = [neighbor.vector]
                    all_neighbors.append(image_neighbors)
                closest_images = list(set.intersection(*all_neighbors))
                print("Intersect",closest_images)
                return render_template('result.html', extraction=feature_extraction, algorithm=algorithm ,uploaded_images=filenames,nearest_images=closest_images, distances=distances)

        elif feature_extraction == 'histogram':     
            #calculate histograms for the images
            for image in filenames:
                print(image)
                input_histograms.append(hc.calculate_for_image(cv.imread(image), [0, 1, 2], (8,12,12), [0,180,0,256,0,256], hsv=True, resize=False))

            if join == 'Union':
                for image in input_histograms:
                    if algorithm == 'KNN':
                        neighbors = histogram_knn.k_nearest(image, neighbors=n_neighbors, distance=distance_method)
                    elif algorithm == 'Range Query':
                        neighbors = histogram_range_query.output(image, max_range=upper_range, distance=distance_method)
                    for neighbor in neighbors:
                        closest_images.append(neighbor.label)
                        if neighbor.label in distances:
                            distances[neighbor.label].append(neighbor.vector)
                        else:
                            distances[neighbor.label] = [neighbor.vector]
                    print(closest_images)
                return render_template('result.html', extraction=feature_extraction, algorithm=algorithm ,uploaded_images=filenames,nearest_images=closest_images, distances=distances)

            if join == 'Intersect':
                all_neighbors = []  
                for image in input_histograms:
                    if algorithm == 'KNN':
                        neighbors = histogram_knn.k_nearest(image, neighbors=n_neighbors, distance=distance_method)
                    elif algorithm == 'Range Query':
                        neighbors = histogram_range_query.output(image, max_range=upper_range, distance=distance_method)
                    image_neighbors = set()
                    for neighbor in neighbors:
                        image_neighbors.add(neighbor.label)
                        if neighbor.label in distances:
                            distances[neighbor.label].append(neighbor.vector)
                        else:
                            distances[neighbor.label] = [neighbor.vector]
                    all_neighbors.append(image_neighbors)
                closest_images = list(set.intersection(*all_neighbors))
                print("Intersect",closest_images)
                return render_template('result.html', extraction=feature_extraction, algorithm=algorithm ,uploaded_images=filenames,nearest_images=closest_images, distances=distances)


    return render_template('index.html')

if __name__ == "__main__":
    load_images()
    app.run(host = '207.180.192.152',debug=False)