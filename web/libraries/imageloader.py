from os import listdir
from os.path import isdir, join, splitext


class ImageLoader():
    def __init__(self):
        self.images = []
        self.image_count = 0
    
    def get_images_root(self, root_folder='', limit=-1):
        for image in listdir(root_folder):
                if(self.image_count >= limit and limit is not -1):
                    break
                if splitext(image)[1] !='.jpg' and splitext(image)[1] !='.png':
                    continue
                self.images.append(join(root_folder, image))
                self.image_count += 1
        return self.image_count, self.images

    def get_images(self, root_folder='', category_max = -1, limit=-1):
        folders = [file for file in listdir(root_folder) if isdir(join(root_folder,file))]
        img_c = 0        
        for folder in folders:
            current_cat = 0
            for image in listdir(join(root_folder,folder)):
                if(img_c >= limit and limit is not -1) or (current_cat>=category_max and category_max is not -1):
                    break
                if splitext(image)[1] !='.jpg' and splitext(image)[1] !='.png':
                    continue
                self.images.append(join(root_folder,folder,image))
                img_c += 1
                current_cat +=1
        self.image_count = len(self.images)
        return self.image_count, self.images