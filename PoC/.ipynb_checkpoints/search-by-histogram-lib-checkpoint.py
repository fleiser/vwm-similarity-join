
# coding: utf-8

# In[1]:


from imageloader import ImageLoader
import numpy as np
import cv2 as cv
from knearestneighbors import KNearestNeighbors
from rangequery import RangeQuery
from matplotlib import pyplot as plt
from histogramcalculation import HistogramCalculation


# In[2]:


number_of_images, images = ImageLoader().get_images('../web/static/dataset/images/vac')


# In[3]:


neighbors = 4
hsv = True
bins = (8,12,12)
channels = [0, 1, 2]
ranges = [0,180,0,256,0,256]
hc = HistogramCalculation()
knn = KNearestNeighbors()


# In[4]:


input_image = cv.imread('../web/static/dataset/images/vac/108105.jpg')
image_input_final = None
if hsv:
    image_input_final = cv.cvtColor(input_image,cv.COLOR_BGR2HSV)
else:
    image_input_final = input_image
input_image_histogram = hc.calculate_for_image(image=image_input_final, channels=channels, bins=bins, ranges=ranges, hsv=hsv)
plt.imshow(cv.cvtColor(input_image,cv.COLOR_BGR2RGB))
plt.show()


# In[ ]:


image_histograms = np.empty([number_of_images,bins[0]*bins[1]*bins[2]])
for index,image in enumerate(images):
    image_histograms[index]=hc.calculate_for_image(cv.imread(image), channels=channels, bins=bins, ranges=ranges,hsv=hsv)
    


# In[ ]:


knn.load(image_histograms,images)


# In[ ]:


for output_image in knn.k_nearest(input_image_histogram):
    print(output_image.vector)
    plt.imshow(cv.cvtColor(cv.imread(output_image.label),cv.COLOR_BGR2RGB))
    plt.show()


# In[ ]:


range_query = RangeQuery(2)
range_query.load(image_histograms, images)


# In[ ]:


range_query.output(input_image_histogram)

