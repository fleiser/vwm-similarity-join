\documentclass{article}

\usepackage[english]{babel}
\usepackage[blindtext]{}
\usepackage{index}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{listings}
\lstset{language=Python}

\makeindex

\begin{document}

\title{\large{\textbf{BI-WVM - Image Similarity Join}}}
\author{Jakub Fleiser}
\maketitle

\newpage
\tableofcontents
\newpage

\section{About}
\subsection{Introduction}
This project focuses on the problem of searching image datasets without any context assigned to the images and a set of images beign the input, the searches are made purely on similiarity as determined by the used algorithms and extracted features from the images. Two feature extraction methods have been used SIFT and colour histograms.
\subsection{Goal}
The goal was to create code modules capable of extracting features from images, ranking those images by similarity, to observe how different approaches or settings affect the performance and quality of searches, as well as a functional web application providing a way for a user to interact with said modules.
\subsection{Result}
I have explored two different approaches of feature extraction from the images, the first one being colour histogram extraction and the second one being the SIFT algorithm with descriptor and keypoint extraction. The algorithms used to determine the most similar images are K-Nearest neighbours and Range query. The output of this project is a series of experiments looking at different advantages and disadvantages of certain approaches, code modules and a web application which takes images as input and retrieves the most similar images from its stored dataset as per criteria defined on input and settings observed to be the best during experimentation.
\newpage

\section{Solution}
\subsection{Feature extraction}
In order to determine if images are similar, features need to be extracted so that there is a meaningful way to express similarity numerically. In this project two different methods have been explored, first being colour histograms and the second one being the Scale Invariant Feature Transformation.
\subsubsection{Colour Histograms}
Colour histograms offer a simple and efficient way of extracting meaningful features from images. An image is taken and for each of the image colour channels colours are counted and a histogram is calculated. This histogram is then normalized and used as a final feature. Normalization happens to avoid the size of the image affecting similarity.
\subsubsection{Scale Invariant Feature Transformation and Bag-of-words model}
The SIFT algorithm calculates a variable length set of key points, for each of those key points the close area around them is described by a descriptor. Key points and descriptors alone offer a poor way of meaningfully comparing images. To resolve this issue a slightly more complicated approach is needed. First, for each of the images in the stored dataset, descriptors are calculated, these are stacked together into a multidimensional array, over this array k-means clustering is performed in order to cluster the descriptors into visual words. The k selected determines the number of visual words that will be created. The resulting set of visual words is called a codebook. Then for each image in the dataset, the extracted descriptors are decoded according to the codebook and occurrences of different visual words are saved in a histogram and normalised to account for different amounts of descriptors SIFT generates. These histograms are when talking in the context of dataset images referenced to as a vocabulary. The same approach of encoding descriptors using the codebook and calculating a visual word descriptor is used for images that are part of the query.
\newpage
\subsection{Searching the dataset}
After extracting the features the dataset needs to be searched for images most similar to the input image. There are two options of doing so, K-Nearest neighbours and range query. Both algorithms use distance metrics to enumerate similarity. In this project Chi-squared, Euclidian and Hausdorff metrics have been implemented.
\subsubsection{K-Nearest Neighbours}
The K-Nearest Neighbours or shortly KNN is a simple algorithm that computes the distance between each image in the dataset and the input image, k closest images are then returned.
\subsubsection{Range Query}
Range Query is not much different from KNN, once again the distance between the input and dataset images is calculated and all images within the on input specified range are returned. The number of images is entirely dependant on the range chosen and can potentially return no or all images from the dataset. Range query requires the user to have an idea of what a good range value might be.
\newpage
\section{Implementation}
\subsection{Language and Frameworks}
The language of choice for this project is Python. Python has good support for both prototyping and final implementation, it has a great number of available and relevant libraries that can be used.
\subsection{Dataset}
The dataset used is the INRIA Holidays dataset containing around 800 images as well as some of my personal photos used for purely for input. The dataset can be found here: http://lear.inrialpes.fr/people/jegou/data.php
\subsection{Libraries}
\begin{itemize}
\item Flask framework - Web framework
\item Numpy - Array operations
\item OpenCV2 - Feature extraction
\item SciPy - K-means clustering, visual word encoding
\item os, sys - interacting with host os
\item functools - set operations
\item Werkzeug.utils - secure uploaded file saving
\end{itemize}
\newpage
\subsection{Web Application}
The web application is written using the flask micro framework. Structure of the app is very simple, on startup, the app looks for numpy binary files containing pre-calculated histograms, codebook, vocabulary and descriptors, if the application fails to find any of these files it calculates them and continues the startup, this is quite undesirable as this action can take a significant amount of time. Once the application loads the necessary data it becomes available for use, it has two pages, one for uploading images and defining the query and one for displaying the results.

\begin{figure}

\caption{Web application upload}
\includegraphics[width=6cm]{images/face.png}
\caption{Web application result}
\includegraphics[width=6cm]{images/out.png}
\end{figure}

\subsection{Algorithm implementation}
\subsubsection{KNN}
The K-Nearest neighbours algorithm implementation consists of a Class KSample which represents a single sample within KNN and the KNearestNeighbors class.

\begin{lstlisting}[breaklines]
class KSample():
    def __init__(self, vector, label):
        self._label=label
        self._vector=vector

    @property
    def label(self):
        return self._label

    @property
    def vector(self):
        return self._vector

    def __str__(self):
        return ("label: " + str(self._label) +  "| vector: " +str(self.vector))
\end{lstlisting}
\newpage
\begin{lstlisting}[breaklines]
class KNearestNeighbors():
    def __init__(self, kneighbors = 3):
        self.neighbors = kneighbors
        self.distances = []
        self.samples = []

    def distance_hausdorff(self, vector_1, vector_2):
        max_distance = 0
        for point_1 in vector_1:
            shortest = float('inf')
            for point_2 in vector_2:
                distance = abs(point_1 - point_2)
                if distance < shortest:
                    shortest = distance
            if shortest > max_distance:
                max_distance = shortest
        return max_distance

    def distance_euclidian(self, vector_1, vector_2):
        return np.sqrt(np.sum((vector_1 - vector_2) ** 2))

    def distance_chi(self, vector_1, vector_2):
        return 0.5 * np.sum([((a - b) ** 2) / (a + b + 1e-10) for (a, b) in zip(vector_1, vector_2)])

    def load(self, X, label):
        if(len(X) != len(label)):
            raise IndexError
        for index, sample in enumerate(X):
            self.samples.append(KSample(X[index],label[index]))
            
    def k_nearest(self, input_vector, distance='chi2', neighbors=-1):
        
        if neighbors == -1:
            output_length = self.neighbors
        else:
            output_length = neighbors
        output = []
        
        for index, sample in enumerate(self.samples):
            new_sample = None
            if distance=='chi2':
                new_sample = (KSample(self.distance_chi(input_vector, sample.vector), sample.label))
            elif distance =='euclidian':
                new_sample = (KSample(self.distance_euclidian(input_vector, sample.vector), sample.label))
            elif distance =='hausdorff':
                new_sample = (KSample(self.distance_hausdorff(input_vector, sample.vector), sample.label))
            output = self.add_to_output(new_sample, output, output_length)
        print(output)
        self.distances = []
        return output
    
    def add_to_output(self, sample, output, k=-1):
        if len(output) < k:
            insert_index = 0
            for index, neighbor in enumerate(output):
                if neighbor.vector < sample.vector:
                    insert_index += 1
                else:
                    insert_index = index            
            output.insert(insert_index, sample)
        else:
            for index, neighbor in enumerate(output):
                if neighbor.vector < sample.vector:
                    continue
                else:
                    insert_index = index
                    output.insert(index, sample)
                    output = output[:-1]
                    break
        return output
\end{lstlisting}
\newpage
\newpage
\subsubsection{Range Query}
The design of the Range query module is similar to the K-Nearest neighbours algorithm. Once again a class is used to represent a data sample in this case RSample and a RangeQuery class to represent the algorithm itself.

\begin{lstlisting}[breaklines]
class RSample():
    def __init__(self, vector, label):
        self._label=label
        self._vector=vector

    @property
    def label(self):
        return self._label

    @property
    def vector(self):
        return self._vector

    def __str__(self):
        return ("label: " + str(self._label) +  "| vector: " +str(self.vector))
\end{lstlisting}
\newpage
\begin{lstlisting}[breaklines]
class RangeQuery():
    def __init__(self, range):
        self.range = range
        self.distances = []
        self.samples = []

    def distance_hausdorff(self, vector_1, vector_2):
        max_distance = 0
        for point_1 in vector_1:
            shortest = float('inf')
            for point_2 in vector_2:
                distance = abs(point_1 - point_2)
                if distance < shortest:
                    shortest = distance
            if shortest > max_distance:
                max_distance = shortest
        return max_distance

    def distance_euclidian(self, vector_1, vector_2):
        return np.sqrt(np.sum((vector_1 - vector_2) ** 2))

    def distance_chi(self, vector_1, vector_2):
        return 0.5 * np.sum([((a - b) ** 2) / (a + b + 1e-10) for (a, b) in zip(vector_1, vector_2)])

    def load(self, X, label):
        if(len(X) != len(label)):
            raise IndexError
        for index, sample in enumerate(X):
            self.samples.append(RSample(X[index],label[index]))
            
    def output(self, input_vector, distance='chi2', max_range=-1):
        for index, sample in enumerate(self.samples):
            if distance=='chi2':
                self.distances.append(RSample(self.distance_chi(input_vector, sample.vector), sample.label))
            if distance=='euclidian':
                self.distances.append(RSample(self.distance_euclidian(input_vector, sample.vector), sample.label))
            if distance=='euclidian':
                self.distances.append(RSample(self.distance_hausdorff(input_vector, sample.vector), sample.label))

        distances_sorted = sorted(self.distances, key=lambda x: x.vector)
        output = []
        if max_range == -1:
            for i in range(0, len(distances_sorted)):
                if(distances_sorted[i].vector>self.range):
                    break
                output.append(distances_sorted[i])
        else:
            for i in range(0, len(distances_sorted)):
                if(distances_sorted[i].vector>max_range):
                    break
                output.append(distances_sorted[i])
        self.distances = []
        return output
\end{lstlisting}
\newpage
\subsection{Colour Histograms}
The colour histogram module is defined by the HistogramCalculation class. The class has two methods, one for single image input and one for multiple images that iterate over a set of images. The methods allow specifying colour channels, colour ranges, histogram bins and image resizing.

\begin{lstlisting}[breaklines]
class HistogramCalculation():
    def __init__(self):
        pass

    def calculate_for_image(self, image, channels, bins, ranges, hsv, segments=1,resize=False, height=300, width=300):
        image_final = None
        if hsv:
            image_final = cv.cvtColor(image,cv.COLOR_BGR2HSV)
        else:
            image_final = image
        if resize:
            image_final = cv.resize(image_final, (width, height), interpolation = cv.INTER_AREA)
        image_shape = image_final.shape
        if(segments == 1):
            image_histogram = cv.calcHist([image_final], channels, None, bins, ranges)
            normalized_image_histogram = cv.normalize(image_histogram, image_histogram)
            return normalized_image_histogram.flatten()
        elif  (segments == 6):
            segments = ((0,0,int(image_shape[1]/3),int(image_shape[0]/2)),
                (int(image_shape[1]/3),0,int((image_shape[1]/3)*2),int(image_shape[0]/2)),
                (int((image_shape[1]/3)*2),0,image_shape[1],int(image_shape[0]/2)),
                    ## SECOND LINE ##
                (0,int(image_shape[0]/2),int(image_shape[1]/3),image_shape[0]),
                (int(image_shape[1]/3),int(image_shape[0]/2),int((image_shape[1]/3)*2),image_shape[0]),
                (int((image_shape[1]/3)*2),int(image_shape[0]/2),image_shape[1],image_shape[0]),
            )
            features = []
            for (x1, y1, x2, y2) in segments:
                mask = np.zeros(image_final.shape[:2], dtype = "uint8")
                cv.rectangle(mask, (x1, y1), (x2, y2), 255, -1)
                image_masked_histogram = cv.calcHist([image_final],channels, mask, bins, ranges)
                masked_normalized_image_histogram = cv.normalize(image_masked_histogram, image_masked_histogram)
                features.extend(masked_normalized_image_histogram.flatten())
            return features
        raise Exception("Invalid input")
        

    def calculate_for_set(self, images, channels, bins, ranges, hsv):
        set_features = []
        for image in images:
            set_features.append(self.calculate_for_image(image, channels, bins, ranges, hsv))
        return set_features
\end{lstlisting}
\newpage
\subsection{SIFT}

The SIFT feature extraction is defined by the SIFTCalculation class. It allows calculating descriptors for a single image, calculating the codebook and word vocabulary (visual word histograms for the input dataset) from a list of descriptors and to calculate a visual word histogram from an image.

\begin{lstlisting}[breaklines]
import cv2 as cv
import numpy as np
from scipy.cluster.vq import vq,kmeans

class SIFTCalculation():
    def __init__(self, number_of_features):
        self.vocabulary = []
        self.images = []
        self.sift = cv.xfeatures2d.SIFT_create(nfeatures=number_of_features)
        self.code_book = []
        self.distortion = []
        self.number_of_words = 0
        pass

    def add_code_book(self, code_book):
        self.code_book = code_book
        self.number_of_words = code_book.shape[0]


    def calculate_sift_descriptors(self, image_path, resize=False, height=300, width=300):
        image = cv.imread(image_path)
        if resize:
            image = cv.resize(image, (width, height), interpolation = cv.INTER_AREA)        
        kpts = self.sift.detect(image)
        kpts, des = self.sift.compute(image, kpts)
        if type(des) != np.ndarray:
            raise Exception(image_path,"Failed CV2 DES")
        return des

    def calculate_vocabulary(self, image_descriptors, k=100):
        descriptors = image_descriptors[0]
        for index, image_descriptor in enumerate(image_descriptors[1:]):
            descriptors = np.vstack((descriptors, image_descriptor))
        
        self.code_book,distortion = kmeans(descriptors, k)
        self.number_of_words = self.code_book.shape[0]
        
        vocabulary = np.zeros((len(image_descriptors), self.number_of_words))
        for index, image_descriptor in enumerate(image_descriptors):
            image_histogram = np.zeros((self.number_of_words))
            words, distances = vq(image_descriptor, self.code_book)
            for word in words:
                image_histogram[word] +=1
            vocabulary[index] = image_histogram
        vocabulary = cv.normalize(vocabulary,vocabulary)
        return vocabulary

    def calculate_word_histogram_for_image(self, image, resize=False, height=300, width=300):
        input_descriptros = self.calculate_sift_descriptors(image, resize, height, width)
        input_histogram = np.zeros((self.number_of_words))
        words, distances = vq(input_descriptros, self.code_book)
        for word in words:
            input_histogram[word] += 1
        input_histogram = cv.normalize(input_histogram,input_histogram)
        input_histogram = cv.normalize(input_histogram,input_histogram)
        return input_histogram
\end{lstlisting}

\newpage
\section{Examples}
This chapter shows selected examples of search queries.
\subsection{SIFT Example}

\begin{figure}[ht]
\centering
\caption{SIFT input, 1000 descriptors, 100 code words}
\includegraphics[width=4cm]{images/knn_sift_pyramid_in.jpg}
\end{figure}
\begin{figure}[ht]
\centering
\caption{SIFT KNN output}
\includegraphics[width=2.5cm]{images/knn_sift_pyramid_out1.jpg}
\includegraphics[width=2.5cm]{images/knn_sift_pyramid_out2.jpg}
\includegraphics[width=2.5cm]{images/knn_sift_pyramid_out3.jpg}
\includegraphics[width=2.5cm]{images/knn_sift_pyramid_out4.jpg}
\end{figure}


\subsection{Histogram Examples}
\begin{figure}[ht]
\centering
\caption{Histogram input, 3 channels, HSV, (8, 12, 12) bins}
\includegraphics[width=4cm]{images/knn_histogram_winery_in.jpg}
\end{figure}
\begin{figure}[ht]
\centering
\caption{Histogram KNN output}
\includegraphics[width=2.5cm]{images/knn_histogram_winery_out1.jpg}
\includegraphics[width=2.5cm]{images/knn_histogram_winery_out2.jpg}
\includegraphics[width=2.5cm]{images/knn_histogram_winery_out3.jpg}
\includegraphics[width=2.5cm]{images/knn_histogram_winery_out4.jpg}
\end{figure}

\begin{figure}[ht]
\centering
\caption{Histogram input, 3 channels, HSV, (8, 12, 12) bins}
\includegraphics[width=4cm]{images/knn_histogram_muntains_in.jpg}
\end{figure}
\begin{figure}[ht]
\centering
\caption{Histogram KNN output}
\includegraphics[width=2.5cm]{images/knn_histogram_muntains_out1.jpg}
\includegraphics[width=2.5cm]{images/knn_histogram_muntains_out2.jpg}
\includegraphics[width=2.5cm]{images/knn_histogram_muntains_out3.jpg}
\includegraphics[width=2.5cm]{images/knn_histogram_muntains_out4.jpg}
\end{figure}

\begin{figure}[ht]
\centering
\caption{Histogram input, 3 channels, HSV, (8, 12, 12) bins}
\includegraphics[width=4cm]{images/knn_histogram_forest_in.jpg}
\end{figure}
\begin{figure}[ht]
\centering
\caption{Histogram KNN output}
\includegraphics[width=2.5cm]{images/knn_histogram_forest_out1.jpg}
\includegraphics[width=2.5cm]{images/knn_histogram_forest_out2.jpg}
\includegraphics[width=2.5cm]{images/knn_histogram_forest_out3.jpg}
\includegraphics[width=2.5cm]{images/knn_histogram_forest_out4.jpg}
\end{figure}


\newpage
\section{Experiments}
The attempted experiments focus on finding a balance between satisfactory outputs and good performance with both colour histograms and SIFT. With SIFT the changing variables are the upper range of descriptors extracted (SIFT doesn't always return the same amount of features), the number of visual code words and size. With Colour Histograms the problem is much simpler with resizing images being the only option.
\subsection{SIFT - 1000 descriptors, original size}
This method proved the most resiliant, altough also quite slow, the disadventage is that SIFT itself is quite slow and computation of descriptors for a single image took multiple seconds.
\begin{figure}
\centering
\includegraphics[width=12cm]{images/sift_times.png}
\caption{SIFT feature extraction time per image in dataset}
\end{figure}
\subsection{SIFT - 500 descriptors, original size}
This method once again doesn't offer great speeds even though the amount of time spent per image is drastically reduced. There is a noticable decrase in quality of searches.
\begin{figure}
\centering
\includegraphics[width=12cm]{images/sift_times_500.png}
\caption{SIFT feature extraction time per image in dataset}
\end{figure}
\subsection{SIFT - 1000 descriptors, resized}
With this method there is no significant change in the amount of time spent per image as compared to the non-resized method with 1000 descriptors and 100 code words. Beyond that, the quality of searches decrases drastically.
\begin{figure}
\centering
\includegraphics[width=12cm]{images/sift_times_1000_resized.png}
\caption{SIFT feature extraction time per image in dataset}
\end{figure}
\subsection{Colour Histogram, original size}
This method provides satisfactory results with decent performance, with the mean time of feature extraction taking 0.282316 seconds (calculated on the entire dataset).
\begin{figure}
\centering
\caption{Colour Histogram feature extraction time per image in dataset}
\includegraphics[width=12cm]{images/histogram_times.png}
\end{figure}
\subsection{Colour Histogram, resized}
This method does on average save some time over original size histograms, with average time being 0.202229 seconds, but is very insignificant and in my subjective opinion the results, while satisfactory are not as good as with original size histograms. Perhaps using a different algorithm for image interpolation (currently open cv's INTER AREA) could improve the results.

\section{Discussion}
In this project I have faced a couple of issues, the first that comes to mind is the subjective nature of "similar images", there is no clear way to tell what is similar and what is not, this makes it rather difficult to evaluate results and different options of feature extraction, this gets worse with the colour histograms method of feature extraction, this method looks purely on colours present in the image but fails to notice any deeper meaning in the picture leading to results that share colours but may be pictures of completely different things. This problem could be potentially reduced if the developed application served a specific purpose or had a "theme" and results became less subjective. Another issue is with a vast amount of unexplored feature extraction options (number of descriptors, image sizes, number of code words, histogram bin ranges...) especially with SIFT, the best configuration is probably still "out there", that is if there is one. Finally, the last obstacle that comes to mind is the lack of data, I have little doubt of usefulness of the methods I used, however, the rather small size of the dataset means that a lot of results are disappointing. Using a larger dataset would definitely lead to improvements.
\begin{figure}
\centering
\caption{Histogram image input}
\includegraphics[width=6cm]{images/histogram_faults_1.jpg}
\caption{Histogram output - similiar colours but vastly different from input}
\includegraphics[width=6cm]{images/histogram_faults_2.jpg}
\end{figure}
\section{Conclusion}
To conclude, in this project two methods of feature extraction were explored, code modules were created for both of them and they were successfully integrated into a flask web application. 
\end{document}
