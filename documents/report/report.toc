\babel@toc {english}{}
\contentsline {section}{\numberline {1}About}{3}% 
\contentsline {subsection}{\numberline {1.1}Introduction}{3}% 
\contentsline {subsection}{\numberline {1.2}Goal}{3}% 
\contentsline {subsection}{\numberline {1.3}Result}{3}% 
\contentsline {section}{\numberline {2}Solution}{4}% 
\contentsline {subsection}{\numberline {2.1}Feature extraction}{4}% 
\contentsline {subsubsection}{\numberline {2.1.1}Colour Histograms}{4}% 
\contentsline {subsubsection}{\numberline {2.1.2}Scale Invariant Feature Transformation and Bag-of-words model}{4}% 
\contentsline {subsection}{\numberline {2.2}Searching the dataset}{5}% 
\contentsline {subsubsection}{\numberline {2.2.1}K-Nearest Neighbours}{5}% 
\contentsline {subsubsection}{\numberline {2.2.2}Range Query}{5}% 
\contentsline {section}{\numberline {3}Implementation}{6}% 
\contentsline {subsection}{\numberline {3.1}Language and Frameworks}{6}% 
\contentsline {subsection}{\numberline {3.2}Dataset}{6}% 
\contentsline {subsection}{\numberline {3.3}Libraries}{6}% 
\contentsline {subsection}{\numberline {3.4}Web Application}{7}% 
\contentsline {subsection}{\numberline {3.5}Algorithm implementation}{7}% 
\contentsline {subsubsection}{\numberline {3.5.1}KNN}{7}% 
\contentsline {subsubsection}{\numberline {3.5.2}Range Query}{11}% 
\contentsline {subsection}{\numberline {3.6}Colour Histograms}{14}% 
\contentsline {subsection}{\numberline {3.7}SIFT}{16}% 
\contentsline {section}{\numberline {4}Examples}{18}% 
\contentsline {subsection}{\numberline {4.1}SIFT Example}{18}% 
\contentsline {subsection}{\numberline {4.2}Histogram Examples}{18}% 
\contentsline {section}{\numberline {5}Experiments}{19}% 
\contentsline {subsection}{\numberline {5.1}SIFT - 1000 descriptors, original size}{19}% 
\contentsline {subsection}{\numberline {5.2}SIFT - 500 descriptors, original size}{19}% 
\contentsline {subsection}{\numberline {5.3}SIFT - 1000 descriptors, resized}{19}% 
\contentsline {subsection}{\numberline {5.4}Colour Histogram, original size}{19}% 
\contentsline {subsection}{\numberline {5.5}Colour Histogram, resized}{20}% 
\contentsline {section}{\numberline {6}Discussion}{20}% 
\contentsline {section}{\numberline {7}Conclusion}{21}% 
