import numpy as np
import sys

class KSample():
    def __init__(self, vector, label):
        self._label=label
        self._vector=vector

    @property
    def label(self):
        return self._label

    @property
    def vector(self):
        return self._vector

    def __str__(self):
        return ("label: " + str(self._label) +  "| vector: " +str(self.vector))

class KNearestNeighbors():
    def __init__(self, kneighbors = 3):
        self.neighbors = kneighbors
        self.distances = []
        self.samples = []

    def distance_hausdorff(self, vector_1, vector_2):
        max_distance = 0
        for point_1 in vector_1:
            shortest = float('inf')
            for point_2 in vector_2:
                distance = abs(point_1 - point_2)
                if distance < shortest:
                    shortest = distance
            if shortest > max_distance:
                max_distance = shortest
        return max_distance

    def distance_euclidian(self, vector_1, vector_2):
        return np.sqrt(np.sum((vector_1 - vector_2) ** 2))

    def distance_chi(self, vector_1, vector_2):
        return 0.5 * np.sum([((a - b) ** 2) / (a + b + 1e-10) for (a, b) in zip(vector_1, vector_2)])

    def load(self, X, label):
        if(len(X) != len(label)):
            raise IndexError
        for index, sample in enumerate(X):
            self.samples.append(KSample(X[index],label[index]))
            
    def k_nearest(self, input_vector, distance='chi2', neighbors=-1):
        
        if neighbors == -1:
            output_length = self.neighbors
        else:
            output_length = neighbors
        output = []
        
        for index, sample in enumerate(self.samples):
            new_sample = None
            if distance=='chi2':
                new_sample = (KSample(self.distance_chi(input_vector, sample.vector), sample.label))
            elif distance =='euclidian':
                new_sample = (KSample(self.distance_euclidian(input_vector, sample.vector), sample.label))
            elif distance =='hausdorff':
                new_sample = (KSample(self.distance_hausdorff(input_vector, sample.vector), sample.label))
            output = self.add_to_output(new_sample, output, output_length)
        print(output)
        self.distances = []
        return output
    
    def add_to_output(self, sample, output, k=-1):
        if len(output) < k:
            insert_index = 0
            for index, neighbor in enumerate(output):
                if neighbor.vector < sample.vector:
                    insert_index += 1
                else:
                    insert_index = index            
            output.insert(insert_index, sample)
        else:
            for index, neighbor in enumerate(output):
                if neighbor.vector < sample.vector:
                    continue
                else:
                    insert_index = index
                    output.insert(index, sample)
                    output = output[:-1]
                    break
        return output