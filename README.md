## Image similiarity search ##

An image search engine built using Python Flask web framework and OpenCV computer vision library. 
This project contains a functional SIFT and Colour Histogram visual bag of words model accesible through a web interface. 
Provided with an set of images on the input, finds the most similiar images in its database using KNN or Range Query.

Full latex report on the project: [Link](documents/report/report.pdf)