import numpy as np

class RSample():
    def __init__(self, vector, label):
        self._label=label
        self._vector=vector

    @property
    def label(self):
        return self._label

    @property
    def vector(self):
        return self._vector

    def __str__(self):
        return ("label: " + str(self._label) +  "| vector: " +str(self.vector))

class RangeQuery():
    def __init__(self, range):
        self.range = range
        self.distances = []
        self.samples = []

    def distance_hausdorff(self, vector_1, vector_2):
        max_distance = 0
        for point_1 in vector_1:
            shortest = float('inf')
            for point_2 in vector_2:
                distance = abs(point_1 - point_2)
                if distance < shortest:
                    shortest = distance
            if shortest > max_distance:
                max_distance = shortest
        return max_distance

    def distance_euclidian(self, vector_1, vector_2):
        return np.sqrt(np.sum((vector_1 - vector_2) ** 2))

    def distance_chi(self, vector_1, vector_2):
        return 0.5 * np.sum([((a - b) ** 2) / (a + b + 1e-10) for (a, b) in zip(vector_1, vector_2)])

    def load(self, X, label):
        if(len(X) != len(label)):
            raise IndexError
        for index, sample in enumerate(X):
            self.samples.append(RSample(X[index],label[index]))
            
    def output(self, input_vector, distance='chi2', max_range=-1):
        for index, sample in enumerate(self.samples):
            if distance=='chi2':
                self.distances.append(RSample(self.distance_chi(input_vector, sample.vector), sample.label))
            if distance=='euclidian':
                self.distances.append(RSample(self.distance_euclidian(input_vector, sample.vector), sample.label))
            if distance=='euclidian':
                self.distances.append(RSample(self.distance_hausdorff(input_vector, sample.vector), sample.label))

        distances_sorted = sorted(self.distances, key=lambda x: x.vector)
        output = []
        if max_range == -1:
            for i in range(0, len(distances_sorted)):
                if(distances_sorted[i].vector>self.range):
                    break
                output.append(distances_sorted[i])
        else:
            for i in range(0, len(distances_sorted)):
                if(distances_sorted[i].vector>max_range):
                    break
                output.append(distances_sorted[i])
        self.distances = []
        return output