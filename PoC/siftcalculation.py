import cv2 as cv
import numpy as np
from scipy.cluster.vq import vq,kmeans

class SIFTCalculation():
    def __init__(self, number_of_features):
        self.vocabulary = []
        self.images = []
        self.sift = cv.xfeatures2d.SIFT_create(nfeatures=number_of_features)
        self.code_book = []
        self.distortion = []
        self.number_of_words = 0
        pass

    def add_code_book(self, code_book):
        self.code_book = code_book
        self.number_of_words = code_book.shape[0]


    def calculate_sift_descriptors(self, image_path, resize=False, height=300, width=300):
        image = cv.imread(image_path)
        if resize:
            image = cv.resize(image, (width, height), interpolation = cv.INTER_AREA)
        kpts = self.sift.detect(image)
        kpts, des = self.sift.compute(image, kpts)
        return des

    def calculate_vocabulary(self, image_descriptors, k=100):
        descriptors = image_descriptors[0]
        for image_descriptor in image_descriptors[1:]:
            descriptors = np.vstack((descriptors, image_descriptor))
        
        self.code_book,distortion = kmeans(descriptors, k)
        self.number_of_words = self.code_book.shape[0]
        
        vocabulary = np.zeros((len(image_descriptors), self.number_of_words))
        for index, image_descriptor in enumerate(image_descriptors):
            image_histogram = np.zeros((self.number_of_words))
            words, distances = vq(image_descriptor, self.code_book)
            for word in words:
                image_histogram[word] +=1
            vocabulary[index] = image_histogram
        vocabulary = cv.normalize(vocabulary,vocabulary)
        return vocabulary

    def calculate_word_histogram_for_image(self, image, resize=False, height=300, width=300):
        input_descriptros = self.calculate_sift_descriptors(image, resize, height, width)
        input_histogram = np.zeros((self.number_of_words))
        words, distances = vq(input_descriptros, self.code_book)
        for word in words:
            input_histogram[word] += 1
        input_histogram = cv.normalize(input_histogram,input_histogram)
        input_histogram = cv.normalize(input_histogram,input_histogram)
        return input_histogram