import cv2 as cv
import numpy as np

class HistogramCalculation():
    def __init__(self):
        pass

    def calculate_for_image(self, image, channels, bins, ranges, hsv, segments=1,resize=False, height=300, width=300):
        image_final = None
        if hsv:
            image_final = cv.cvtColor(image,cv.COLOR_BGR2HSV)
        else:
            image_final = image
        if resize:
            image_final = cv.resize(image_final, (width, height), interpolation = cv.INTER_AREA)
        image_shape = image_final.shape
        if(segments == 1):
            image_histogram = cv.calcHist([image_final], channels, None, bins, ranges)
            normalized_image_histogram = cv.normalize(image_histogram, image_histogram)
            return normalized_image_histogram.flatten()
        elif  (segments == 6):
            segments = ((0,0,int(image_shape[1]/3),int(image_shape[0]/2)),
                (int(image_shape[1]/3),0,int((image_shape[1]/3)*2),int(image_shape[0]/2)),
                (int((image_shape[1]/3)*2),0,image_shape[1],int(image_shape[0]/2)),
                    ## SECOND LINE ##
                (0,int(image_shape[0]/2),int(image_shape[1]/3),image_shape[0]),
                (int(image_shape[1]/3),int(image_shape[0]/2),int((image_shape[1]/3)*2),image_shape[0]),
                (int((image_shape[1]/3)*2),int(image_shape[0]/2),image_shape[1],image_shape[0]),
            )
            features = []
            for (x1, y1, x2, y2) in segments:
                mask = np.zeros(image_final.shape[:2], dtype = "uint8")
                cv.rectangle(mask, (x1, y1), (x2, y2), 255, -1)
                image_masked_histogram = cv.calcHist([image_final],channels, mask, bins, ranges)
                masked_normalized_image_histogram = cv.normalize(image_masked_histogram, image_masked_histogram)
                features.extend(masked_normalized_image_histogram.flatten())
            return features
        raise Exception("Invalid input")
        

    def calculate_for_set(self, images, channels, bins, ranges, hsv):
        set_features = []
        for image in images:
            set_features.append(self.calculate_for_image(image, channels, bins, ranges, hsv))
        return set_features